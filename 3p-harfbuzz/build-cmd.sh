#!/bin/bash

# turn on verbose debugging output for parabuild logs.
set -x
# make errors fatal
set -e

TOP="$(dirname "$0")"
PROJECT=harfbuzz
LICENSE=README
VERSION=$(ls  $TOP/source | grep .gz | cut -b10-14)
SOURCE_DIR="$PROJECT"


if [ -z "$AUTOBUILD" ] ; then 
    fail
fi

# load autbuild provided shell functions and variables
set +x
eval "$("$AUTOBUILD" source_environment)"
set -x

if [ "$OSTYPE" = "cygwin" ] ; then
    autobuild="$(cygpath -u $AUTOBUILD)"
else
    autobuild="$AUTOBUILD"
fi

stage="$(pwd)"



#HARFBUZZ_SOURCE_DIR="$PROJECT"
#VERSION="$(sed -n 's/^ *VERSION=\([0-9.]*\)$/\1/p' "../$HARFBUZZ_SOURCE_DIR/configure")"

# load autbuild provided shell functions and variables
source_environment_tempfile="${stage}/source_environment.sh"
"$autobuild" source_environment > "$source_environment_tempfile"
. "$source_environment_tempfile"

build=${AUTOBUILD_BUILD_ID:=0}

echo "${VERSION}.${build}" > "${stage}/VERSION.txt"
case "$AUTOBUILD_PLATFORM" in
    "linux")
        # Prefer gcc-4.9 if available.
        
            export CC=/usr/bin/gcc
            export CXX=/usr/bin/g++
       

        # Default target to 32-bit
        opts="${TARGET_OPTS:--m32}"
        # Handle any deliberate platform targeting
        if [ -z "$TARGET_CPPFLAGS" ]; then
            # Remove sysroot contamination from build environment
            unset CPPFLAGS
        else
            # Incorporate special pre-processing flags
            export CPPFLAGS="$TARGET_CPPFLAGS"
        fi
        pushd "$TOP/$SOURCE_DIR"
           export PKG_CONFIG_PATH="$stage/packages/lib/pkgconfig"
           CFLAGS="$opts -O3 -fPIC -DPIC -I$stage/packages/include -I$GLIB_INCLUDE -I$CAIRO_INCLUDE -I$FREETYPE_INCLUDE"  
            CC="$CC -m32" ./configure --prefix="$stage" 
			make
            make install
		    mkdir -p "$stage/include/harfbuzz"
		    cp -a src/*.h "$stage/include/harfbuzz"
        popd

        mv lib release
        mkdir -p lib
        mv release lib
    ;;
    "linux64")
        # Prefer gcc-6 if available.
            export CC=/usr/bin/gcc
            export CXX=/usr/bin/g++
        # Default target to 64-bit
        opts="${TARGET_OPTS:--m64}"
        # Handle any deliberate platform targeting
        if [ -z "$TARGET_CPPFLAGS" ]; then
            # Remove sysroot contamination from build environment
            unset CPPFLAGS
        else
            # Incorporate special pre-processing flags
            export CPPFLAGS="$TARGET_CPPFLAGS"
        fi
        pushd "$TOP/$SOURCE_DIR"
           export PKG_CONFIG_PATH="$stage/packages/lib/pkgconfig"
           CFLAGS="$opts -O3 -fPIC -DPIC"  
            CC="$CC -m64" ./configure --prefix="$stage" 
			make
            make install
		    mkdir -p "$stage/include/harfbuzz"
		    cp -a src/*.h "$stage/include/harfbuzz"
        popd

        mv lib release
        mkdir -p lib
        mv release lib
    ;;
    *)
        echo "platform not supported"
        exit -1
    ;;
esac


mkdir -p "$stage/LICENSES"
cp "$TOP/$SOURCE_DIR/$LICENSE" "$stage/LICENSES/$PROJECT.txt"




