#!/usr/bin/env bash

# turn on verbose debugging output for parabuild logs.
set -x
# make errors fatal
set -e

TOP="$(dirname "$0")"

DB_VERSION="5.1.25"
DB_SOURCE_DIR="db-$DB_VERSION"


if [ -z "$AUTOBUILD" ] ; then 
    exit 1
fi

if [ "$OSTYPE" = "cygwin" ] ; then
    autobuild="$(cygpath -u $AUTOBUILD)"
else
    autobuild="$AUTOBUILD"
fi

stage="$(pwd)"

"$autobuild" source_environment > "$stage/variables_setup.sh" || exit 1
. "$stage/variables_setup.sh"

case "$AUTOBUILD_PLATFORM" in
    linux*)
        pushd "$TOP/$DB_SOURCE_DIR/build_unix"
            LDFLAGS="-m$AUTOBUILD_ADDRSIZE -std=c++11" CFLAGS="-m$AUTOBUILD_ADDRSIZE -std=c++11" CXXFLAGS="-m$AUTOBUILD_ADDRSIZE -std=c++11" ../dist/configure --prefix="$stage" --target=x86_64-linux-gnu
            make
            make install
        popd
        mv lib release
        mkdir -p lib
        mv release lib
    ;;
    *)
        exit -1
    ;;
esac


mkdir -p "$stage/LICENSES"
cp "$TOP/$DB_SOURCE_DIR/LICENSE" "$stage/LICENSES/db.txt"
echo "$DB_VERSION" > "$stage/VERSION.txt"


